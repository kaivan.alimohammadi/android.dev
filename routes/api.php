<?php
return [
	'/api/v1/users'         => 'Application\Models\User::getUsers',
	'/api/v1/product'       => 'Application\Models\Product::getProduct',
	'/api/v1/products'      => 'Application\Models\Product::getProducts',
	'/api/v1/order/create'  => 'Application\Models\Order::createOrder',
	'/api/v1/orders'        => 'Application\Models\Order::getOrders',
	'/api/v1/order'         => 'Application\Models\Order::getOrder',
	'/api/v1/basket/add'    => 'Application\Models\Basket::addItem',
	'/api/v1/basket/remove' => 'Application\Models\Basket::removeItem',
	'/api/v1/basket'        => 'Application\Models\Basket::getBasket',
	'/api/v1/payment/add'   => 'Application\Models\Payment::createPayment',
	'/api/v1/token'         => 'Application\Services\Token::parseToken',
];