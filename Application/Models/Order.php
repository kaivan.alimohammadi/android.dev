<?php

namespace Application\Models;


use Application\Repositories\OrderItemRepository;
use Application\Repositories\OrderRepository;

class Order {

	public static function createOrder() {
		$newOrderData        = [
			'order_user_id'       => (int) $_POST['order_user_id'],
			'order_status'        => (int) $_POST['order_status'],
			'order_payable_price' => (int) $_POST['order_payable_price'],
			'order_total_price'   => (int) $_POST['order_total_price'],
			'order_purchase_type' => (int) $_POST['order_purchase_type']
		];
		$repository          = new OrderRepository();
		$orderItemRepository = new OrderItemRepository();

		$order_id = $repository->create( $newOrderData, [ '%d', '%d', '%d', '%d', '%d' ] );
		if ( intval( $order_id ) > 0 ) {
			$orderItems = [
				[
					'order_item_order_id'   => $order_id,
					'order_item_product_id' => 2,
					'order_item_count'      => 1,
					'order_item_price'      => 150000,
					'order_item_discount'   => 0
				],
				[
					'order_item_order_id'   => $order_id,
					'order_item_product_id' => 3,
					'order_item_count'      => 1,
					'order_item_price'      => 210000,
					'order_item_discount'   => 0
				]
			];
			foreach ( $orderItems as $orderItem ) {
				$orderItemRepository->create( $orderItem, [ '%d', '%d', '%d', '%d', '%d' ] );
			}
		}

	}

	public static function getOrders() {
		return ( new OrderRepository() )->all( [ 'order_id', 'order_user_id' ] );
	}

	public static function getOrder() {
		$order_id = isset( $_POST['order_id'] ) && intval( $_POST['order_id'] ) > 0 ? intval( $_POST['order_id'] ) : null;
		if ( $order_id ) {
			return ( new OrderRepository() )->find( $order_id );
		} else {
			return [
				'status'  => false,
				'message' => 'شناسه سفارش معتبر نمی باشد'
			];
		}

	}

}