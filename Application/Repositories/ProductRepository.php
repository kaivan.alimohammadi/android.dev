<?php
namespace Application\Repositories;

class ProductRepository extends BaseRepository {

	public function __construct() {
		parent::__construct();
		$this->table = $this->db->prefix.'products';
		$this->primaryKey = 'product_id';
		$this->perPage = 20;
	}

	public function transformer() {
	}

}