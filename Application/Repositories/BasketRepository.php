<?php

namespace Application\Repositories;


use Application\Models\Basket;

class BasketRepository extends BaseRepository {
	public function __construct() {
		parent::__construct();
		$this->table      = $this->db->prefix . 'basket';
		$this->primaryKey = 'basket_item_id';
	}

	public function updateCount( int $product_id, int $type, int $count ) {
		$operator  = $type == Basket::INCREASE ? '+' : '-';
		$sql_query = "UPDATE {$this->table} SET basket_item_count= basket_item_count {$operator} {$count} WHERE basket_item_product_id={$product_id} LIMIT 1";

		return $this->db->query( $sql_query );
	}

	public function getAllItems() {
		$product_table = 'wp_products';
		return $this->db->get_results("
			SELECT 
			basket.*,
			product.product_title
			FROM {$this->table} basket
			JOIN {$product_table} product ON basket.basket_item_product_id=product.product_id
		");
	}
}